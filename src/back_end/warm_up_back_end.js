import React, { useState } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

export const WarmUpBackEnd = props => {
  const [string, setString] = useState();
  function onChangeValue(event) {
    const string = event.target.value;
    setString(string);
  }
  return (
    <div style={{ margin: 50 }}>
      <TextField
        id="string"
        label="input string"
        value={string}
        onChange={res => onChangeValue(res)}
        margin="normal"
      />
      <div>
        <text>Hello,{string}!</text>
      </div>
      <div style={{ margin: 20 }}>
        <Button
          variant="contained"
          onClick={() => {
            props.history.push("/sort");
          }}
        >
          Next answer
        </Button>
      </div>
    </div>
  );
};
