import React from "react";
import Button from "@material-ui/core/Button";
export const SortLastName = (props) => {
  const exampleData = [
    { firstName: "Michael", lastName: "Jordan" },
    { firstName: "Christian", lastName: "Ronaldo" },
    { firstName: "Bill", lastName: "Gates" },
    { firstName: "Steve", lastName: "Jobs" },
    { firstName: "Michael", lastName: "Schumacher" },
    { firstName: "Frodo", lastName: "Baggins" },
    { firstName: "Barack", lastName: "Obama" },
    { firstName: "Tiger", lastName: "Woods" }
  ];

  function mapArray() {
    return exampleData.map(newData => (
      <div style={{ flexDirection: "row" }}>
        <text>
          {newData.firstName},{newData.lastName}
        </text>
      </div>
    ));
  }
  function sortLastName() {
    const afterSort = exampleData.sort((firstDate, secondData) =>
      firstDate.lastName > secondData.lastName ? 1 : -1
    );
    return afterSort.map(newData => (
      <div style={{ flexDirection: "row" }}>
        <text>
          {newData.firstName},{newData.lastName}
        </text>
      </div>
    ));
  }
  return (
    <div>
      {mapArray()}
      <h1 style={{ marginTop: 50 }}>After Sort </h1>
      {sortLastName()}
      <div style={{ margin: 20 }}>
        <Button
          variant="contained"
          onClick={() => {
            props.history.push("/warmupfrontend");
          }}
        >
          Next answer
        </Button>
      </div>
    </div>
  );
};
