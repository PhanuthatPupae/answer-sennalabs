import React from "react";
import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { WarmUpBackEnd } from "./back_end/warm_up_back_end";
import { SortLastName } from "./back_end/answer1";
import { WarmUpFrontEnd } from "./front_end/warm_up_front_end";
import { TodoApp } from "./front_end/answer1_front_end";
import { FindUniq } from "./front_end/answer2_front_end";
function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={WarmUpBackEnd} />
          <Route exact path="/sort" component={SortLastName} />
          <Route exact path="/warmupfrontend" component={WarmUpFrontEnd} />
          <Route exact path="/todoapp" component={TodoApp} />
          <Route exact path="/finduniq" component={FindUniq} />
          <Route component={WarmUpBackEnd} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
