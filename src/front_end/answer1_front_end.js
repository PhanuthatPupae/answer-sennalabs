import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Snackbar from "@material-ui/core/Snackbar";
export const TodoApp = props => {
  const exampleData = [
    { detail: "Buy KitKat", id: "1a", delete: true },
    { detail: "Go to Senna Labs at 2 P.M.", id: "2c", delete: false },
    { detail: "Workout at 5 P.M.", id: "3b", delete: false },
    { detail: "Give a flower to June", id: "5v", delete: false }
  ];
  const pushData = { detail: "", id: "", delete: false };
  const useStyles = makeStyles(theme => ({
    root: {
      width: "100%",
      marginTop: theme.spacing(10),
      overflowX: "auto"
    },
    table: {
      minWidth: 650
    },
    button: {
      margin: theme.spacing(1)
    },
    textField: {
      width: 500
    },
    input: {
      display: "none"
    }
  }));
  const [newData, setNewData] = useState(pushData);
  const [state, setState] = useState(exampleData);
  const [isOpen, setIsOpen] = useState(false);
  const [message, setMessage] = useState("");
  const classes = useStyles();
  function mapRowTable() {
    return state.map(newData => (
      <TableRow key={newData.id}>
        <TableCell
          component="th"
          scope="row"
          style={checkIsDelete(newData.delete)}
        >
          {newData.detail}
        </TableCell>
        <TableCell align="right">
          <Button
            color="primary"
            className={classes.button}
            onClick={() => handleOpen("DONE")}
          >
            DONE
          </Button>
          <Button
            style={{ color: "#F1D53D" }}
            className={classes.button}
            onClick={() => handleOpen("EDIT")}
          >
            EDIT
          </Button>
          <Button
            color="secondary"
            className={classes.button}
            onClick={() => isDelete(newData)}
          >
            DELETE
          </Button>
        </TableCell>
      </TableRow>
    ));
  }
  function checkIsDelete(isDelete) {
    if (isDelete) {
      return { textDecoration: " line-through" };
    }
  }
  function handleChange(detail) {
    const value = detail.target.value;
    setNewData({ detail: value, id: state.length, delete: false });
  }
  function handleOpen(message) {
    setIsOpen(!isOpen);
    setMessage(message)
  }
  function isDelete(props) {
    if (props) {
      if (props.id) {
        const afterUpdate = state.map(element => {
          const id = element.id;
          if (id === props.id) {
            return { ...element, delete: true };
          } else return element;
        });
        setState(afterUpdate);
      }
    }
  }

  function getAlert() {
    return (
      <Snackbar
        anchorOrigin={{
          vertical: "top",
          horizontal: "center"
        }}
        open={isOpen}
        autoHideDuration={1000}
        message={message}
        onClose={() => setIsOpen(!isOpen)}
      />
    );
  }
  return (
    <div style={{ marginLeft: 50, marginRight: 50, marginBottom: 50 }}>
      <div>
        <TextField
          id="standard-name"
          label="Enter what you want to do here..."
          className={classes.textField}
          value={newData.detail}
          onChange={detail => handleChange(detail)}
          margin="normal"
        />
        <Button
          color="primary"
          className={classes.button}
          style={{ marginTop: 30 }}
          onClick={() => {
            setState([...state, newData]);
          }}
        >
          ADD
        </Button>
      </div>

      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell>Detail</TableCell>
              <TableCell align="right">Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>{mapRowTable()}</TableBody>
        </Table>
      </Paper>
      <div style={{ margin: 20 }}>
        <Button
          variant="contained"
          onClick={() => {
            props.history.push("/finduniq");
          }}
        >
          Next answer
        </Button>
      </div>
      {getAlert()}
    </div>
  );
};
